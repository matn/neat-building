from neat.reporting import BaseReporter
from neat import nsga
import os
from multiprocessing import Pool
from common import create_voxels, create_voxels_distance
import numpy as np
import codecs, json
import pickle
from neat.hypervolume import HyperVolume

class FrontReporter(BaseReporter):
    def __init__(self, width, height, depth, full_scale, box_size, target, root="results/"):
        BaseReporter.__init__(self)
        self.root = root
        self._path = os.path.join(root, "fronts", "generation_{i}")
        self.width = width
        self.height = height
        self.depth = depth
        self.full_scale = full_scale
        self.target = target
        self.box_size = box_size
        self.hv = HyperVolume([0., 0.])
        with open(os.path.join(self.root, "hypervolume.txt"), 'w') as f:
            s = "generation, hypervolume, best_f1, best_f2, num_ind\n"
            f.write(s)

    def post_sorting(self, species, generation, config):
        print("outputing results from generation %i in..." % generation)
        print(self._path.format(i=generation))

        path = self._path.format(i=generation)
        if not os.path.exists(path):
            os.makedirs(path)
        global_fronts = []
        pool = Pool(4)
        jobs = []

        for s in species.values():
            global_fronts.extend(s.fronts[1])

        sorted_fronts = nsga.fast_non_dominated_sort(global_fronts)
        all_fitnesses = [f[1].fitness for f in sorted_fronts[1]]
        hv = self.hv.compute(all_fitnesses)
        all_fitnesses = np.array(all_fitnesses)

        with open(os.path.join(self.root, "hypervolume.txt"), 'a') as f:
            s = "%i, %f, %f, %f, %i\n" % (generation, hv, np.min(all_fitnesses[:,0]), np.min(all_fitnesses[:,1]), len(sorted_fronts[1]))
            f.write(s)

        for _, genome in sorted_fronts[1]:

            jobs.append(
                pool.apply_async(
                    create_voxels, (
                        genome, config,
                        self.width * self.full_scale,
                        self.height * self.full_scale,
                        self.depth * self.full_scale
                    )
                )
            )

        for (genome, j) in zip(sorted_fronts[1], jobs):
            voxels = np.array(j.get()).astype(np.uint8) + 2 * self.target
            self.writeIndividual(voxels, path, genome[1], genome[1].key)

        pool.close()

    def writeIndividual(self,voxels, path, genome, idx=0):
        dic = {
            'voxels' : voxels.tolist(),
            'fitness': genome.fitness.tolist(),
            'boxSize': self.box_size.tolist(),
            'id': genome.key
        }
        ind = "voxels_%i.json" % idx
        gen = "genome_%i" %idx

        json.dump(dic, open(os.path.join(path,ind), 'w'))
        pickle.dump(genome, open(os.path.join(path, gen),'w'))
