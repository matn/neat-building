import os
import random
from multiprocessing import Pool

import numpy as np
import neat
import pickle
from common import *

width, height, depth  = 16, 16, 16
full_scale = 1
distance_enabled = True
penalty = 0.1

def distanceFromTarget(voxels, target, **kwargs):


def eval_voxel(fitness_func, genome, config, kwargs):
    if distance_enabled:
        voxels = create_voxels_distance(genome, config, width, height, depth)
    else:
        voxels = create_voxels(genome, config, width, height, depth)

    return fitness_func(genome, target, kwargs)



class NoveltyEvaluator(object):
    def __init__(self, num_workers):
        self.num_workers = num_workers
        self.pool = Pool(num_workers)
        self.archive = []
        self.out_index = 1


    def evaluate(self, genomes, config):
        jobs = []
        for genome_id, genome in genomes:
            jobs.append(self.pool.apply_async(eval_voxel, (genome, config)))

        new_archive_entries = []
        for (genome_id, genome), j in zip(genomes, jobs):
            genome.fitness = (width * height * width)
            fitness_max = genome.fitness
            voxels = np.array(j.get()).astype(np.uint8)

            for a in self.archive:
                adist = np.sum(np.abs(voxels- a))
                genome.fitness = min(genome.fitness, adist)

            if np.sum(voxels) > 0.8 * fitness_max or np.sum(voxels) < 0.1 * fitness_max:
                genome.fitness = genome.fitness * penalty

            if random.random() < 0.02:
                new_archive_entries.append(voxels)
                volume = eval_voxel(genome, config)

                vol = np.array(volume).astype(np.uint8)
                filename = 'archive/novelty-%i.npy' % self.out_index
                np.save(filename, vol)

                self.out_index += 1


        self.archive.extend(new_archive_entries)
        print('{0} archive entries'.format(len(self.archive)))


def run():
    # Determine path to configuration file.
    local_dir = os.path.dirname(__file__)
    if distance_enabled:
        config_path=os.path.join(local_dir, 'voxel_config_distance')
    else:
        config_path = os.path.join(local_dir, 'voxel_config')
    # Note that we provide the custom stagnation class to the Config constructor.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_path)

    ne = NoveltyEvaluator(4)

    pop = neat.Population(config)

    # Add a stdout reporter to show progress in the terminal.
    pop.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)

    test = 1
    gen = 0
    with open('config', 'w') as f:
        pickle.dump(config, f)


    while test:
        gen +=1
        pop.run(ne.evaluate, 1)

        winner = stats.best_genome()
        volume = eval_voxel(
            winner,
            config,
        )


        vol = np.array(volume).astype(np.uint8)
        filename = 'winner/winner-%i' % ne.out_index
        np.save(filename, vol)

        volume = np.array(vol).astype(np.uint8)
        ne.archive.append(volume)
        with open('winner/genome_%i'% gen, 'w') as f:
            pickle.dump(winner, f)


if __name__ == '__main__':
    run()
