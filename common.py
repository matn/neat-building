import neat
import numpy as np
import matplotlib.pyplot as plt
import itertools
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage import measure

activation_threshold = 0



def check_if_closed(vertices):
    num_vertices = len(vertices)

    count = 0
    for i in range(num_vertices):
        for j in range(i+1, num_vertices):
            if (vertices[i] == vertices[j]).all():
                count += 1


    if count >= num_vertices/3:
        return 1
    else:
        return 0

def count_edges(triangles):
    dic = {}
    for t in triangles:
        edges = list(itertools.permutations(t,2))
        for e in edges:
            if dic.get(e):
                dic[e] += 1
            else:
                dic[e] = 1

    return dic


def create_cube(width, height, depth):
    cube = np.zeros((width, height, depth))
    for i in range(width):
        for j in range(height):
            for k in range(depth):
                if ((i == 0 or i == width -1) or
                    (j == 0 or j == height-1) or
                    (k == 0 or k == depth -1)):
                    cube[i,j,k] = 1

    return cube

def create_sphere(width, height, depth, r):

    if r > width/2 or r > height/2 or r/height/2:
        raise ValueError
    else:
        sphere = np.zeros((width, height, depth))
        center = np.array([width-1, height-1, depth-1])/2
        for i in range(width):
            for j in range(height):
                for k in range(depth):
                    vox = np.array([i,j,k])
                    if voxel_distance(vox, center) < r:
                        sphere[i,j,k] = 1

    return sphere


def voxel_distance(v1, v2):
    return np.sum(np.abs(v2 - v1))

def create_filter(width, height, depth):
    arr = np.ones((width, height, depth))

    for i in range(width):
        for j in range(height):
            for k in range(depth):
                arr[i,j,k] = min(
                    min(i,j,k),
                    min(
                        (width - 1 - i),
                        (height - 1 - j),
                        (depth - 1 -k)
                    )
                )

    return (arr.max() - arr) / arr.max()

def create_voxels_distance(genome, config, width, height, depth, bias = 0.):
    net = neat.nn.RecurrentNetwork.create(genome, config)
    volume = []
    center = np.array([width/2,height/2,depth/2])
    for l in range(width):
        x = l / (width - 1.)
        layer = []
        for r in range(height):
            y = r / (height - 1.)
            row = []
            for c in range(depth):
                z = c / (depth - 1.)
                d = voxel_distance(np.array([l,r,c]), center)
                output = net.activate([x,y,z,d, bias])
                vox = 1 if output[0] > activation_threshold else 0
                row.append(vox)
            layer.append(row)
        volume.append(layer)

    return volume



def create_voxels(genome, config, width, height, depth, bias = 0):
    net = neat.nn.RecurrentNetwork.create(genome, config)
    volume = []
    for l in range(width):
        x = l / (width - 1.)
        layer = []
        for r in range(height):
            y = r / (height - 1.)
            row = []
            for c  in range(depth):
                z = c / (depth - 1.)
                output = net.activate([x,y,z, bias])
                vox = 1 if output[0] > activation_threshold else 0
                row.append(vox)
            layer.append(row)
        volume.append(layer)

    return volume

def compute_volume(vertices, triangles):
    volume = 0.
    for t in triangles:
        q = np.ones((4,4))
        q[0:3,0:3] = vertices[t]
        volume += np.linalg.det(q)/6

    return abs(volume)

def compute_surface(vertices, triangles):
    area = 0.
    for t in triangles:
        a, b, c  = vertices[t]
        area += np.linalg.norm(np.cross(b-a, c-a))/2.
    return area

def compute_compacity(vertices, triangles):
    volume = compute_volume(vertices, triangles)
    area = compute_surface(vertices, triangles)
    return np.power(volume, 2)/np.power(area, 3)

def plot(arr, c=None):
    if arr.min() == arr.max():
        f = create_filter(*arr.shape)
        arr = arr * f

    if not c:
        vertices, faces, _, _ = measure.marching_cubes_lewiner(arr)
    else:
        vertices, faces, _, _ = measure.marching_cubes_lewiner(arr, c)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    mesh = Poly3DCollection(vertices[faces])
    mesh.set_edgecolor('k')
    ax.add_collection3d(mesh)

    ax.set_xlim(0, arr.shape[0])
    ax.set_ylim(0, arr.shape[1])
    ax.set_zlim(0, arr.shape[2])

    plt.tight_layout()
    plt.show()
