import os
import random
from multiprocessing import Pool
import itertools

import numpy as np
import neat
from skimage import measure
from scipy.stats import multivariate_normal
from common import create_voxels, compute_compacity, create_filter, check_if_closed, count_edges


width, height, depth  = 8, 8, 8
full_scale = 1
root = "out"
num_gen = 300
compacity_max = 1/(36*np.pi)

class VoxelEvaluator(object):
    def __init__(self, num_workers):
        self.num_workers = num_workers
        self.pool = Pool(num_workers)
        self.archive = []
        self.out_index = 1


    def evaluate(self, genomes, config):
        jobs = []
        f = create_filter(width, height, depth)
        for genome_id, genome in genomes:
            jobs.append(
                self.pool.apply_async(
                    create_voxels, (genome, config, width, height, depth)
                )
            )

        new_archive_entries = []
        for (genome_id, genome), j in zip(genomes, jobs):
            voxels = np.array(j.get()).astype(np.uint8)
            is_open = False
            max_fitness = 0.
            if voxels.max() == 0:
                genome.fitness = 0.
            else:
                if voxels.min() == 1.:
                    voxels = f * voxels

                vertices, triangles,_,_ = measure.marching_cubes_lewiner(voxels)
                edges = count_edges(triangles)

                if check_if_closed(vertices) or min(edges.values()) > 1:
                    genome.fitness = compute_compacity(vertices, triangles)/compacity_max
                else:
                    genome.fitness = 0.

            if genome.fitness > max_fitness:
                max_fitness = genome.fitness


        for (genome_id, genome), j in zip(genomes, jobs):
            if genome.fitness == max_fitness:
                filename = 'winner-%i' % self.out_index
                np.save(filename, voxels)


def run():
    # Determine path to configuration file.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'voxel_config')
    # Note that we provide the custom stagnation class to the Config constructor.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_path)

    ne = VoxelEvaluator(4)

    pop = neat.Population(config)

    # Add a stdout reporter to show progress in the terminal.
    pop.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)
    for i in range(num_gen):
        pop.run(ne.evaluate, 1)

        winner = stats.best_genome()
        volume = create_voxels(
            winner,
            config,
            full_scale * width,
            full_scale * height,
            full_scale * depth
        )

        ne.out_index +=1


if __name__ == '__main__':
    run()
