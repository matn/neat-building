import os
import random
import json
import codecs
from multiprocessing import Pool

import numpy as np
import neat
import pickle
from common import *

full_scale = 1
distance_enabled = True
penalty = 1.
target_file = 'rat.json'


# load target
with open(target_file, 'r') as target:
    target_dict = json.load(target)

target = np.array(target_dict['voxels'])
boxSize = np.array(target_dict['boxSize'])

width, height, depth = target.shape
archive_path = 'results/match_rat/archive/'
archive_file = os.path.join(archive_path, 'individual_%i.json')
winner_path = 'results/match_rat/winner/'
winner_file = os.path.join(winner_path, 'winner_%s_%i.json')

print(width, height, depth)
print target

bias = 1

def eval_voxel(genome, config, scale = 1):
    if distance_enabled:
        return create_voxels_distance(
            genome,
            config,
            width * scale,
            height * scale,
            depth * scale,
            bias)
    else:
        return create_voxels(genome, config, width, height, depth, bias)


class NoveltyEvaluator(object):
    def __init__(self, num_workers):
        self.num_workers = num_workers
        self.pool = Pool(num_workers)
        self.archive = []
        self.out_index = 1


    def evaluate(self, genomes, config):
        jobs = []
        for genome_id, genome in genomes:
            jobs.append(self.pool.apply_async(eval_voxel, (genome, config)))

        new_archive_entries = []
        for (genome_id, genome), j in zip(genomes, jobs):
            genome.fitness = (width * height * width)
            fitness_max = genome.fitness
            voxels = np.array(j.get()).astype(np.uint8)

            adist = np.sum(np.logical_not(np.logical_xor(voxels, target)))

            # adist = -np.sum(np.abs(voxels- target))
            genome.fitness = min(genome.fitness, adist)

            if np.sum(voxels) > 0.8 * fitness_max or np.sum(voxels) < 0.1 * fitness_max:
                genome.fitness = genome.fitness * penalty

            if random.random() < 0.02:
                new_archive_entries.append(voxels)
                dic = {
                    'voxels' : voxels.tolist(),
                    'fitness': float(genome.fitness),
                    'boxSize': boxSize.tolist()
                }
                with codecs.open(archive_file % self.out_index, 'w', encoding='utf-8') as f:
                    json.dump(dic, f)

                self.out_index += 1


        self.archive.extend(new_archive_entries)
        print('{0} archive entries'.format(len(self.archive)))


def run():
    # Determine path to configuration file.
    local_dir = os.path.dirname(__file__)
    if not os.path.exists(archive_path):
        os.makedirs(archive_path)

    if not os.path.exists(winner_path):
        os.makedirs(winner_path)
    if distance_enabled:
        config_path=os.path.join(local_dir, 'voxel_config_distance')
    else:
        config_path = os.path.join(local_dir, 'voxel_config')
    # Note that we provide the custom stagnation class to the Config constructor.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_path)

    with open(winner_file % ('config', 0), 'w') as f:
        pickle.dump(config, f)

    ne = NoveltyEvaluator(4)

    pop = neat.Population(config)

    # Add a stdout reporter to show progress in the terminal.
    pop.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    pop.add_reporter(stats)

    gen = 0
    with open('config', 'w') as f:
        pickle.dump(config, f)


    for i in range(300):
        gen +=1
        pop.run(ne.evaluate, 1)

        winner = stats.best_genome()
        volume = eval_voxel(
            winner,
            config,
            scale = 1
        )


        dic = {
            'voxels' : volume,
            'fitness': float(winner.fitness),
            'boxSize': boxSize.tolist()
        }

        dic2 = {
            'voxels' : (np.array(volume) * 2 + target).tolist(),
            'fitness': float(winner.fitness),
            'boxSize': boxSize.tolist()
        }

        with codecs.open(winner_file %('voxels',gen), 'w', encoding='utf-8') as f:
            json.dump(dic, f)

        with codecs.open(winner_file %('overlapse',gen), 'w', encoding='utf-8') as f:
            json.dump(dic2, f)

        with open(winner_file % ('genome', gen), 'w') as f:
            pickle.dump(winner, f)

    with open(winner_file % ('statistics', gen), 'w') as f:
        pickle.dump(winner, f)


if __name__ == '__main__':
    run()
