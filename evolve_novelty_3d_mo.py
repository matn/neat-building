import os
import random
from multiprocessing import Pool
import itertools
import pickle
import numpy as np
import neat
from neat import nsga
from skimage import measure
from scipy.stats import multivariate_normal
from common import create_voxels, compute_compacity, create_filter, check_if_closed, count_edges, create_sphere, create_voxels_distance
import json
from reporters import FrontReporter


width, height, depth  = 20, 10, 8
full = float(width * height * depth)
full_scale = 1

root = "results/rat_novelty_200gen_2_recurrent"
archive = os.path.join(root, "archive")

num_gen = 200
target2 = create_sphere(width,height,depth, 4)
rat = json.load(open('rat.json', 'r'))
target = np.array(rat['voxels'])
penalty = 0.1
boxSize = np.array([1.,1.,1.])

def distance(one, other):
    return np.sum(np.logical_xor(one, other))

def similarity(one, other):

    return np.sum(np.logical_not(np.logical_xor(one, other)))

def distanceFromArchive(voxels, archive):
    fitness = width * height * depth
    for other in archive:
        fitness = min(fitness, distance(voxels, other))
    return fitness

def computeFitness(voxels, archive):
    fitness = [0., 0.]
    fitness[0] = similarity(voxels, target)
    if len(archive) != 0:
        fitness[1] = distanceFromArchive(voxels, archive)
    else:
        fitness[1] = 0.

    return -np.array(fitness)/full

def computeFitnessTwoTargets(voxels):
    fitness = [0., 0.]
    full = np.ones((width, height, depth))
    empty = np.zeros((width, height, depth))

    fitness[0] = similarity(voxels, target)
    fitness[1] = similarity(voxels, target2)

    if similarity(full, target) >= fitness[0] or similarity(empty, target) >= fitness[0]:
        fitness[0] = fitness[0] * penalty

    if similarity(full, target2) >= fitness[1] or similarity(empty, target2) >= fitness[1]:
        fitness[1] = fitness[1] * penalty

    return -np.array(fitness) / float(width * height * depth)




def init():
    if not os.path.exists(archive):
        os.makedirs(archive)
    if not os.path.exists(fronts):
        os.makedirs(fronts)

class VoxelEvaluator(object):
    def __init__(self, num_workers):
        self.num_workers = num_workers
        self.pool = Pool(num_workers)
        self.archive = []
        self.out_index = 1


    def evaluate(self, genomes, config):
        jobs = []
        f = create_filter(width, height, depth)
        for genome_id, genome in genomes:
            jobs.append(
                self.pool.apply_async(
                    create_voxels, (genome, config, width, height, depth)
                )
            )

        new_archive_entries = []
        for (genome_id, genome), j in zip(genomes, jobs):
            voxels = np.array(j.get()).astype(np.uint8)
            is_open = False
            max_fitness = 0.
            genome.fitness = computeFitness(voxels, self.archive)
            full = width*height*depth
            if np.sum(voxels) > 0.8 * full or np.sum(voxels) < 0.1 * full:
                genome.fitness = genome.fitness * penalty


            self.archive.extend(new_archive_entries)

def run():
    # Determine path to configuration file.

    if not os.path.exists(archive):
        os.makedirs(archive)

    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'voxel_config_distance')
    # Note that we provide the custom stagnation class to the Config constructor.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_path)
    ne = VoxelEvaluator(1)

    pop = neat.Population(config, multi_objective=True)
    # Add a stdout reporter to show progress in the terminal.
    pop.add_reporter(neat.MOReporter(True))
    fr = FrontReporter(width, height, depth, full_scale, boxSize, target, root)
    pop.add_reporter(fr)
    # stats = neat.StatisticsReporter()
    # pop.add_reporter(stats)
    for i in range(num_gen):
        pop.run(ne.evaluate, 1)
        new_archive_entries = []
        pool = Pool(4)
        jobs = []
        for s in pop.previous[1].species.values():
            new_archive_entries.extend(s.fronts[1])

        for _, genome in new_archive_entries:

            jobs.append(
                pool.apply_async(
                    create_voxels, (
                        genome, config,
                        width,
                        height,
                        depth
                    )
                )
            )

        for (genome, j) in zip(new_archive_entries, jobs):
            voxels = np.array(j.get()).astype(np.uint8)
            if len(ne.archive) > 0:
                s = 0
                for a in ne.archive:
                    s = max(similarity(a, voxels), s)
                if s < height * width * depth:
                    ne.archive.append(voxels)
            else:
                ne.archive.append(voxels)
        pool.close()
        print("archive size: %i" % len(ne.archive))


if __name__ == '__main__':
    run()
